package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class BambooClam implements Clams {

    public String toString() {
        return "Bamboo Clams from North American Atlantic Coast";
    }
}
