package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class EggplantTest {

    private Class<?> eggplantClass;
    private Eggplant eggplant;

    @Before
    public void setUp() throws Exception {
        eggplantClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant");
        eggplant = new Eggplant();
    }

    @Test
    public void testEggplantIsAKindOfVeggie() {
        Collection<Type> classInterfaces = Arrays.asList(eggplantClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testEggplantOverrideToStringMethod() throws Exception {
        Method toString = eggplantClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Eggplant", eggplant.toString());
    }
}