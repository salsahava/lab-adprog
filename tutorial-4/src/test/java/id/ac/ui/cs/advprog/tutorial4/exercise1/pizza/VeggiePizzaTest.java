package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class VeggiePizzaTest {
    private VeggiePizza veggiePizza;

    @Before
    public void setUp() throws Exception {
        veggiePizza = new VeggiePizza(new DepokPizzaIngredientFactory());
    }

    @Test
    public void veggiePizzaExists() {
        assertNotNull(veggiePizza);
    }
}