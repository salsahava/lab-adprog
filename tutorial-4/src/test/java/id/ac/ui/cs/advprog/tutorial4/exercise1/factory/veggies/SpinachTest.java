package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class SpinachTest {

    private Class<?> spinachClass;
    private Spinach spinach;

    @Before
    public void setUp() throws Exception {
        spinachClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach");
        spinach = new Spinach();
    }

    @Test
    public void testSpinachIsAKindOfVeggie() {
        Collection<Type> classInterfaces = Arrays.asList(spinachClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testSpinachOverrideToStringMethod() throws Exception {
        Method toString = spinachClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Spinach", spinach.toString());
    }
}