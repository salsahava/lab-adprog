package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class FreshClamsTest {

    private Class<?> freshClamsClass;
    private FreshClams freshClams;

    @Before
    public void setUp() throws Exception {
        freshClamsClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams");
        freshClams = new FreshClams();
    }

    @Test
    public void testFreshClamIsAKindOfClam() {
        Collection<Type> classInterfaces = Arrays.asList(freshClamsClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams"))
        );
    }

    @Test
    public void testFreshClamOverrideToStringMethod() throws Exception {
        Method toString = freshClamsClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Fresh Clams from Long Island Sound", freshClams.toString());
    }
}