package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class NewYorkPizzaStoreTest {

    private NewYorkPizzaStore newYorkPizzaStore;

    @Before
    public void setUp() throws Exception {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void newYorkPizzaStoreIsWorking() {
        assertNotNull(newYorkPizzaStore.createPizza("cheese"));
        assertNotNull(newYorkPizzaStore.createPizza("veggie"));
        assertNotNull(newYorkPizzaStore.createPizza("clam"));
    }

}