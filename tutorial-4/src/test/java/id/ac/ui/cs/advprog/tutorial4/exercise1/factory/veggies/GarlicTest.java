package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class GarlicTest {

    private Class<?> garlicClass;
    private Garlic garlic;

    @Before
    public void setUp() throws Exception {
        garlicClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic");
        garlic = new Garlic();
    }

    @Test
    public void testGarlicIsAKindOfVeggie() {
        Collection<Type> classInterfaces = Arrays.asList(garlicClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testGarlicOverrideToStringMethod() throws Exception {
        Method toString = garlicClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Garlic", garlic.toString());
    }
}