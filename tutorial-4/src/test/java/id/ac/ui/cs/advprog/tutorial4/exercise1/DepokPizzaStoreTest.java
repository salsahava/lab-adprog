package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DepokPizzaStoreTest {

    private DepokPizzaStore depokPizzaStore;

    @Before
    public void setUp() throws Exception {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void depokPizzaStoreIsWorking() {
        assertNotNull(depokPizzaStore.createPizza("cheese"));
        assertNotNull(depokPizzaStore.createPizza("veggie"));
        assertNotNull(depokPizzaStore.createPizza("clam"));
    }

}