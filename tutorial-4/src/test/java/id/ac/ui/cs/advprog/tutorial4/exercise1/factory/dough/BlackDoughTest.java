package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class BlackDoughTest {

    private Class<?> blackDoughClass;
    private BlackDough blackDough;

    @Before
    public void setUp() throws Exception {
        blackDoughClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.BlackDough");
        blackDough = new BlackDough();
    }

    @Test
    public void testBlackDoughIsAKindOfDough() {
        Collection<Type> classInterfaces = Arrays.asList(blackDoughClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough"))
        );
    }

    @Test
    public void testBlackDoughOverrideToStringMethod() throws Exception {
        Method toString = blackDoughClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Black Dough", blackDough.toString());
    }
}