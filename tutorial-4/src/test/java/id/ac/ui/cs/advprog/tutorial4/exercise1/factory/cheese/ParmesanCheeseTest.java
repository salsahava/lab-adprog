package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class ParmesanCheeseTest {

    private Class<?> parmesanCheeseClass;
    private ParmesanCheese parmesanCheese;

    @Before
    public void setUp() throws Exception {
        parmesanCheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese");
        parmesanCheese = new ParmesanCheese();
    }

    @Test
    public void testParmesanCheeseIsAKindOfCheese() {
        Collection<Type> classInterfaces = Arrays.asList(parmesanCheeseClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese"))
        );
    }

    @Test
    public void testParmesanCheeseOverrideToStringMethod() throws Exception {
        Method toString = parmesanCheeseClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Shredded Parmesan", parmesanCheese.toString());
    }
}