package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class ThickCrustDoughTest {

    private Class<?> thickCrustDoughClass;
    private ThickCrustDough thickCrustDough;

    @Before
    public void setUp() throws Exception {
        thickCrustDoughClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough");
        thickCrustDough = new ThickCrustDough();
    }

    @Test
    public void testThickCrustDoughIsAKindOfDough() {
        Collection<Type> classInterfaces = Arrays.asList(thickCrustDoughClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough"))
        );
    }

    @Test
    public void testThickCrustDoughOverrideToStringMethod() throws Exception {
        Method toString = thickCrustDoughClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("ThickCrust style extra thick crust dough", thickCrustDough.toString());
    }
}