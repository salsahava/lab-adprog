package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class PlumTomatoSauceTest {

    private Class<?> plumTomatoSauceClass;
    private PlumTomatoSauce plumTomatoSauce;

    @Before
    public void setUp() throws Exception {
        plumTomatoSauceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce");
        plumTomatoSauce = new PlumTomatoSauce();
    }

    @Test
    public void testPlumTomatoSauceIsAKindOfSauce() {
        Collection<Type> classInterfaces = Arrays.asList(plumTomatoSauceClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce"))
        );
    }

    @Test
    public void testPlumTomatoSauceSauceOverrideToStringMethod() throws Exception {
        Method toString = plumTomatoSauceClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Tomato sauce with plum tomatoes", plumTomatoSauce.toString());
    }
}