package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class CornTest {

    private Class<?> cornClass;
    private Corn corn;

    @Before
    public void setUp() throws Exception {
        cornClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Corn");
        corn = new Corn();
    }

    @Test
    public void testCornIsAKindOfVeggie() {
        Collection<Type> classInterfaces = Arrays.asList(cornClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testCornOverrideToStringMethod() throws Exception {
        Method toString = cornClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Corns", corn.toString());
    }
}