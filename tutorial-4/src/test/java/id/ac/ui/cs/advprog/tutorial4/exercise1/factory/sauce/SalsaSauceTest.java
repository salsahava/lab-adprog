package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class SalsaSauceTest {

    private Class<?> salsaSauceClass;
    private SalsaSauce salsaSauce;

    @Before
    public void setUp() throws Exception {
        salsaSauceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.SalsaSauce");
        salsaSauce = new SalsaSauce();
    }

    @Test
    public void testSalsaSauceIsAKindOfSauce() {
        Collection<Type> classInterfaces = Arrays.asList(salsaSauceClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce"))
        );
    }

    @Test
    public void testSalsaSauceOverrideToStringMethod() throws Exception {
        Method toString = salsaSauceClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Salsa Sauce", salsaSauce.toString());
    }
}