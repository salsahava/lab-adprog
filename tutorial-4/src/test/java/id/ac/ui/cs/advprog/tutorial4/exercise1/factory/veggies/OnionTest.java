package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class OnionTest {

    private Class<?> onionClass;
    private Onion onion;

    @Before
    public void setUp() throws Exception {
        onionClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion");
        onion = new Onion();
    }

    @Test
    public void testOnionIsAKindOfVeggie() {
        Collection<Type> classInterfaces = Arrays.asList(onionClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testOnionOverrideToStringMethod() throws Exception {
        Method toString = onionClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Onion", onion.toString());
    }
}