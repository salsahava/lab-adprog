package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class MushroomTest {

    private Class<?> mushroomClass;
    private Mushroom mushroom;

    @Before
    public void setUp() throws Exception {
        mushroomClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom");
        mushroom = new Mushroom();
    }

    @Test
    public void testMushroomIsAKindOfVeggie() {
        Collection<Type> classInterfaces = Arrays.asList(mushroomClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testMushroomOverrideToStringMethod() throws Exception {
        Method toString = mushroomClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Mushrooms", mushroom.toString());
    }
}