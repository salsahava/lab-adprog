package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ClamPizzaTest {
    private ClamPizza clamPizza;

    @Before
    public void setUp() throws Exception {
        clamPizza = new ClamPizza(new DepokPizzaIngredientFactory());
    }

    @Test
    public void clamPizzaExists() {
        assertNotNull(clamPizza);
    }
}