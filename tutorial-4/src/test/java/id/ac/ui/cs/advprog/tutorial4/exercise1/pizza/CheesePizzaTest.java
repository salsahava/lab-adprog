package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CheesePizzaTest {
    private CheesePizza cheesePizza;

    @Before
    public void setUp() throws Exception {
        cheesePizza = new CheesePizza(new DepokPizzaIngredientFactory());
    }

    @Test
    public void cheesePizzaExists() {
        assertNotNull(cheesePizza);
    }
}