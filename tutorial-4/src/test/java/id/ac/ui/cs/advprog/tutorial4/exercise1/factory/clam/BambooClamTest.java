package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class BambooClamTest {

    private Class<?> bambooClamClass;
    private BambooClam bambooClam;

    @Before
    public void setUp() throws Exception {
        bambooClamClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.BambooClam");
        bambooClam = new BambooClam();
    }

    @Test
    public void testBambooClamIsAKindOfClam() {
        Collection<Type> classInterfaces = Arrays.asList(bambooClamClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams"))
        );
    }

    @Test
    public void testBambooClamOverrideToStringMethod() throws Exception {
        Method toString = bambooClamClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Bamboo Clams from North American Atlantic Coast", bambooClam.toString());
    }
}