package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class MarinaraSauceTest {

    private Class<?> marinaraSauceClass;
    private MarinaraSauce marinaraSauce;

    @Before
    public void setUp() throws Exception {
        marinaraSauceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce");
        marinaraSauce = new MarinaraSauce();
    }

    @Test
    public void testMarinaraSauceIsAKindOfSauce() {
        Collection<Type> classInterfaces = Arrays.asList(marinaraSauceClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce"))
        );
    }

    @Test
    public void testMarinaraSauceOverrideToStringMethod() throws Exception {
        Method toString = marinaraSauceClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Marinara Sauce", marinaraSauce.toString());
    }
}