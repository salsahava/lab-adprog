package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class BlackOlivesTest {

    private Class<?> blackOlivesClass;
    private BlackOlives blackOlives;

    @Before
    public void setUp() throws Exception {
        blackOlivesClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives");
        blackOlives = new BlackOlives();
    }

    @Test
    public void testBlackOliveIsAKindOfVeggie() {
        Collection<Type> classInterfaces = Arrays.asList(blackOlivesClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testBlackOlivesOverrideToStringMethod() throws Exception {
        Method toString = blackOlivesClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Black Olives", blackOlives.toString());
    }
}