package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class FrozenClamsTest {

    private Class<?> frozenClamsClass;
    private FrozenClams frozenClams;

    @Before
    public void setUp() throws Exception {
        frozenClamsClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams");
        frozenClams = new FrozenClams();
    }

    @Test
    public void testFrozenClamIsAKindOfClam() {
        Collection<Type> classInterfaces = Arrays.asList(frozenClamsClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams"))
        );
    }

    @Test
    public void testFrozenClamOverrideToStringMethod() throws Exception {
        Method toString = frozenClamsClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Frozen Clams from Chesapeake Bay", frozenClams.toString());
    }
}