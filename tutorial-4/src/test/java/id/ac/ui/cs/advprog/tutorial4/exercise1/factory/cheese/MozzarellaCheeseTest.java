package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class MozzarellaCheeseTest {

    private Class<?> mozzarellaCheeseClass;
    private MozzarellaCheese mozza;

    @Before
    public void setUp() throws Exception {
        mozzarellaCheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese");
        mozza = new MozzarellaCheese();
    }

    @Test
    public void testMozzarellaCheeseIsAKindOfCheese() {
        Collection<Type> classInterfaces = Arrays.asList(mozzarellaCheeseClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese"))
        );
    }

    @Test
    public void testMozzarellaCheeseOverrideToStringMethod() throws Exception {
        Method toString = mozzarellaCheeseClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Shredded Mozzarella", mozza.toString());
    }
}