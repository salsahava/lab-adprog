package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.NoCrustSandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThinBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

public class AdprogDiner {
    public static void main(String[] args) {
        Food food = new ThickBunBurger();
        food = new BeefMeat(food);
        food = new Cheese(food);
        food = new Cucumber(food);
        food = new Lettuce(food);
        food = new ChiliSauce(food);
        System.out.println(food.getDescription() + " $" + food.cost());

        Food food1 = new ThinBunBurger();
        food1 = new Tomato(food1);
        food1 = new Lettuce(food1);
        food1 = new Cucumber(food1);
        System.out.println(food1.getDescription() + " $" + food1.cost());

        Food food2 = new CrustySandwich();
        food2 = new BeefMeat(food2);
        food2 = new ChickenMeat(food2);
        food2 = new ChiliSauce(food2);
        food2 = new TomatoSauce(food2);
        System.out.println(food2.getDescription() + " $" + food2.cost());

        Food food3 = new NoCrustSandwich();
        food3 = new BeefMeat(food3);
        food3 = new Cheese(food3);
        food3 = new ChickenMeat(food3);
        food3 = new ChiliSauce(food3);
        food3 = new Cucumber(food3);
        food3 = new Lettuce(food3);
        food3 = new Tomato(food3);
        food3 = new TomatoSauce(food3);
        System.out.println(food3.getDescription() + " $" + food3.cost());
    }
}
