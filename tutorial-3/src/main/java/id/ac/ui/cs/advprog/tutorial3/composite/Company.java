package id.ac.ui.cs.advprog.tutorial3.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Iterator;

public class Company {
    protected List<Employees> employeesList;
    protected double netSalary;

    public Company() {
        employeesList = new ArrayList<Employees>();
    }

    public Company(List<Employees> employeesList) {
        Collections.copy(this.employeesList, employeesList);
    }

    public void addEmployee(Employees employees) {
        employeesList.add(employees);
    }

    public double getNetSalaries() {
        Iterator it = employeesList.iterator();

        while (it.hasNext()) {
            Employees salary = (Employees) it.next();
            netSalary += salary.getSalary();
        }

        return netSalary;
    }

    public List<Employees> getAllEmployees() {
        return employeesList;
    }
}
